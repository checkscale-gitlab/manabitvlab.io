---
title: Cài đặt docker cho windows10
date: 2021-10-11
categories:
 - others
tags:
 - others
---
- Download Docker cho windows10 [tại đây](https://www.docker.com/products/docker-desktop)
- Khi cài đặt nhớ check vào options `Enable WSL 2 Windows Features`
- Nếu WSL2 được cài đặt thành công thì trong phần General, `Use the WSL 2 based engine` sẽ được tick vào.
- Ở trong phần Resources, Enable integration with my default WSL distro cũng đã được tick vào, nếu chưa thì hay kick hoạt nó lên.