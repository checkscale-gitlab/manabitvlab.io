---
title: Security in AWS Organizations
date: 2021-10-19
categories:
 - aws
tags:
 - aws
---
Đảm bảo sự an toàn là mục quan trọng nhất của AWS Cloud. Đảm bảo sự sự an toàn được coi là trách nhiệm chung giữa AWS với khách hàng.

## Trách nhiệm của AWS
- Đảm bảo về cơ sở hạ tầng cho việc chạy ổn định các dịch vụ trên AWS Cloud.
- AWS cung cấp các dịch vụ để giúp khách hàng sử dụng các dịch vụ trên AWS một cách an toàn. 
- Thêm nữa, bằng những phần mềm bên thứ 3, AWS thường xuyên kiểm tra định kỳ tính an toàn của dịch vụ, nó như một phần việc bắt buộc cần thực thi được định nghĩa trong Compliance Program của AWS.

## Trách nhiệm của người dùng
- Phạm vi của phần trách nhiệm này phụ thuộc vào dịch mà khách hàng sử dụng.
- Nó cũng phụ thuộc vào tính nhạy cảm của dữ liệu hay như các luật, các quy định hiện hành.

## Logging và Monitoring
- CloudTrail: Để đảm bảo an toàn cho các dịch vụ trên AWS Organizations, AWS cung cấp dịch vụ CloudTrail để logging tất cả các API gọi đến AWS Organizations. Khách hàng có thể kiểm tra, tìm kiếm, download các lịch sử các sự kiện của một user bất kì nào. 
- CloudWatch: Là dịch vụ để thông báo cho người quản lý biết khi một sự kiện đã được định nghĩa được thực hiện. Sự kiện được định nghĩa ở đây có thể là: khi một user mới được tạo, hoặc một user nào đó rời tổ chức...

## Compliance validation
AWS cung cấp phần mềm thứ 3, hoặc dịch vụ, chỉ dẫn để giúp cho khách hàng thường xuyên kiểm tra được độ an toàn của dịch vụ. Tất cả những thông tin này nằm trong chương trình gọi là Compliance Program.
- [Security and Compliance Quick Start Guide](http://aws.amazon.com/quickstart/?awsf.quickstart-homepage-filter=categories%23security-identity-compliance): đưa ra những bước chỉ dẫn cơ bản để xây dựng một dịch vụ đảm bảo an toàn
- [AWS Compliance Resources](http://aws.amazon.com/compliance/resources/): tổng hợp hướng dẫn để áp dụng security vào từng ngành cụ thể hoặc vị trí địa lý cụ thể
- [Evaluating Resources with Rules](https://docs.aws.amazon.com/config/latest/developerguide/evaluate-config.html): tổng hợp những đánh cho những cài đặt xem những cài đặt đó tốt hay chưa
- [AWS Security Hub](https://docs.aws.amazon.com/securityhub/latest/userguide/what-is-securityhub.html): kiểm tra được trạng thái security so với chuẩn security
- [AWS Audit Manager](https://docs.aws.amazon.com/audit-manager/latest/userguide/what-is.html): đơn giản hóa việc quản lý rủi ro để tuân thủ các quy định và tiêu chuẩn ngành.

### Tham khảo
[Security in AWS Organizations](https://docs.aws.amazon.com/organizations/latest/userguide/security.html)