---
title: Cài đặt môi trường phát triển cho S+camera
date: 2021-10-08
categories:
 - others
tags:
 - others
---
## Xây dựng môi trường phát triển trên PC

Khi cài đặt python lên trên pyenv thì cần những thư viện phụ thuộc. Ta sẽ install nó trước
```bash
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
```
### Cài đặt pyenv
```bash
$ git clone https://github.com/yyuu/pyenv.git $HOME/.pyenv
$ export PYENV_ROOT="$HOME/.pyenv"
$ export PATH="$PYENV_ROOT/bin:$PATH"
$ eval "$(pyenv init -)"
```

### Cài đặt python 3.7.3
```bash
$ wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz
$ tar -xvzf Python-3.7.3.tgz
$ cd Python-3.7.3
$ ./configure --enable-optimizations
$ make -j 8
$ sudo make altinstall
```

### Cài đặt python bằng pyenv
```bash
$ pyenv install 3.7.3
$ pyenv local 3.7.3
$ pyenv rehash
```

### Lưu biến môi trường
```bash
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
$ echo 'eval "$(pyenv init -)"' >> ~/.bashrc
$ source ~/.bashrc
```

### Tạo môi trường venv
```bash
$ sudo mkdir -p /opt/soracom/python/
$ self=$(whoami)
$ sudo chown -R $self /opt/soracom/
$ python3.7 -m venv --without-pip /opt/soracom/python/
$ source /opt/soracom/python/bin/activate
```

## Deploy
### Cài đặt phần mềm
```bash
# for ubuntu amd architecture
$ sudo apt-get install jq
$ wget https://github.com/soracom/soracom-cli/releases/download/v0.10.2/soracom_0.10.2_amd64.deb
$ dpkg -i soracom_0.10.2_amd64.deb
```

### Đăng ký thông tin
```bash
$ LANG=ja soracom configure
# Chọn Japan
# Chọn 3 -> điền các thông tin cần thiết
```

### Kiểm tra thông tin đã đăng ký
```bash
$ soracom configure get
```
### Deploy tools script
Download deploy tool [tại đây](https://drive.google.com/file/d/12kSdp0ksraM4bUCR5avHYu_Xoes8VnFx)

### Run deploy
```bash
$ cd deploy
# Edit mosaic_deploy.sh
# Edit SORACOME_DEVICE_ID="d-m8qk4fo61vbiu4nc0hq6"
# Edit SORACOM_PROFILE="default"
$ ./mosaic_deploy.sh ../s_plus_camera/
```
